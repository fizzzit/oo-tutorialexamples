#include <iostream> //for cout/endl etc.
#include <cstdlib>  //c++ version of stdlib - for exit. 

#include "Vector2f.h"
#include "Colour.h"
#include "PlayerShip.h"
#include "PowerUp.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
    /* PlayerShip needs a Colour object and
     * a Vector2f object. 
     */
    PlayerShip* ship = new PlayerShip();

    /* Having created the object I pass it into 
     * PlayerShip 
     */
    Colour *c = new Colour(255,0,0);
    ship->setColour(c);

    /* The alternative is to create the object 
     * and pass it stright into PlayerShip
     */
    ship->setPosition(new Vector2f(1.1f,2.2f));

    /* NOTE: I delete neither of these, the
     * PlayerShip object takes care of both
     */

    ship->collectPowerUp(new PowerUp("Health Pack"));
    ship->collectPowerUp(new PowerUp("Shield Upgrade"));
    ship->collectPowerUp(new PowerUp("Missile Launcher"));    

    ship->printShipInfo(); 
 
    delete ship;

    exit(0);

}
