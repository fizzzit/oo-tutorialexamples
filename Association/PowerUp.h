#ifndef _POWERUP_H
#define _POWERUP_H

#include <string>
using std::string;

class PowerUp 
{
private:
    string name;
public:
    PowerUp();
    PowerUp(string name); 
    ~PowerUp(); 

    void setName(string name);
    string getName();    
};
#endif
