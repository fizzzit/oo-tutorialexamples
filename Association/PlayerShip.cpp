
#include "PlayserShip.h"
#include "Colour.h"
#include "Vector2f.h"
#include "PowerUp.h"

#include <iostream>
using std::cout;
using std::endl;

const int PlayerShip::MAX_ENERGY = 100;

PlayerShip::PlayerShip()
{
    energy = MAX_ENERGY;
    itemCount = 0;
    colour = nullptr;
    position = nullptr;
}

PlayerShip::PlayerShip(Vector2f* pos, Colour* col)
{
    energy = MAX_ENERGY;
    itemCount = 0;
    setColour(col);
    setPosition(pos);
}

PlayerShip::~PlayerShip()
{
    delete colour;
    colour = nullptr;
    
    delete position;
    position = nullptr;

    for(int i = 0; i < itemCount; i++)
    { 
        delete inventory[i];
    }
}

void PlayerShip::takeHit(int points) 
{
    energy -= points;
}

void PlayerShip::setColour(Colour* value) 
{
    /* This is a little naive, I should really check 
     * there isn't a value here already 
     */
    colour = value;
}

void PlayerShip::setPosition(Vector2f* value) 
{
    /* This is a little naive, I should really check 
     * there isn't a value here already 
     */
    position = value;
}

void PlayerShip::collectPowerUp(PowerUp* powerUp) 
{
     /* Some power ups may change energy, 
      * code to handle this would go here
      */

    if(itemCount < MAX_INVENTORY)
    {     
        inventory[itemCount] = powerUp;
        itemCount++;
    }
}

Colour* PlayerShip::getColour() 
{
    /* This is a very naive, once this pointer has
     * been given some other code it can be
     * changed, i.e. its no longer private 
     */
    return colour;
}

Vector2f* PlayerShip::getPosition() 
{
    /* This is a very naive, once this pointer has
     * been given some other code it can be
     * changed, i.e. its no longer private 
     */
    return position;
}

/* Note: This is only for the class examples 
 * adding test code like this into our classes
 * increases the coupling (e.g. to cout etc.)
 * we'd normally use friend classes or a
 * test framework 
 */
void PlayerShip::printShipInfo()
{
    cout << "** Player Ship **" << endl;
    
    if(colour != nullptr)
        cout << "Colour is:(R: " << +colour->getRed() << 
                " , G: " << +colour->getGreen() << 
                " , B: " << +colour->getBlue() << 
                " )" << endl;

    if(position != nullptr)
        cout << "Position is:(X: " << position->getX() << 
                " , Y: " << position->getY() << 
                " )" << endl;
    
    cout << "Ship Inventory:" << endl;
    for(int i = 0; i < itemCount; i++)
    {
        cout << "\t" << inventory[i]->getName() << endl;
    }
    
}


