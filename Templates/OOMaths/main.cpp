#include <iostream>
#include "Vector2.hpp"

int main(int argc, char* argv[])
{
    std::cout << "Hello World" << std::endl;

    Maths::Vector2<float> v1(1.0f, 2.0f);
    Maths::Vector2<float> v2(3.0f, 3.0f);

    std::cout << "v1 is ";
    std::cout << "[" << v1.x() << "," << v1.y() << "]" << std::endl;

    std::cout << "v2 is ";
    std::cout << "[" << v2.x() << "," << v2.y() << "]" << std::endl;


    v1 = v1 + v2;
    std::cout << "v1 = v1 + v2";
    std::cout << "[" << v1.x() << "," << v1.y() << "]" << std::endl;
    
     // testing overloaded scale. 
    v1 *= 2.0f;
    std::cout << "v1 *= 2.0f"; 
    std::cout << "[" << v1.x() << "," << v1.y() << "]" << std::endl;

}
