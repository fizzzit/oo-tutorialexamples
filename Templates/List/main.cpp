/*
 * main.cpp
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */
#include <iostream>

#include "List.hpp"

#include "CircleList.h"
#include "SquareList.h"

#include "Circle.h"
#include "Square.h"

using std::cout;
using std::endl;

int main(int argc, char* argv[])
{
	// Without templates
	Circle* a = new Circle();
	a->setRadius(5);

	Circle* b = new Circle();
	a->setRadius(4);

	CircleList* circleList = new CircleList();

	circleList->addList(a);
	circleList->addList(b);

	Square* c = new Square();
	c->setLenght(5);

	Square* d = new Square();
	d->setLenght(4);

	SquareList* squareList = new SquareList();

	squareList->addList(c);
	squareList->addList(d);

	// With templates

	Circle* w = new Circle();
	w->setRadius(4);

	Circle* x = new Circle();
	x->setRadius(6);

	MyList< Circle* > *circleListTemplated;
	circleListTemplated = new MyList< Circle* >();

	circleListTemplated->addList(w);
	circleListTemplated->addList(x);

	Square* y = new Square();
	y->setLenght(5);

	Square* z = new Square();
	z->setLenght(4);

	MyList< Square* > *squareListTemplated;
	squareListTemplated = new MyList< Square* >();

	squareList->addList(y);
	squareList->addList(z);

	delete squareList;
	delete circleList;

	delete squareListTemplated;
	delete circleListTemplated;

	return 0;
}