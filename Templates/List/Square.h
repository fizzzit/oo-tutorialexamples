/*
 * Square.h
 *
 *  Created on: 8 Jun 2011
 *      Author: sc-gj
 */

#ifndef SQUARE_H_
#define SQUARE_H_

class Square {
public:
	Square();
	virtual ~Square();
	void setLenght(float);
	float getLenght();
	
private:
	float length;
};

#endif /* SQUARE_H_ */
