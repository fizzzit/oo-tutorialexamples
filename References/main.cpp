#include <iostream>

void doubleNumber(int& a);

int addTwo(const int& a, const int& b);

// Works only for arrays of a given size. 
float average(int (&myArray)[4]);

template <int T>
float average2(int (&myArray)[T]);

int main(int argc, char* argv[])
{
    std::cout << "Hello World" << std::endl;

    int x = 10;

    std::cout << "Original value of x is " << x << std::endl;

    doubleNumber(x);

    std::cout << "Double of x is " << x << std::endl;

    std::cout << "Adding 3 + 4 gives " << addTwo(3,4) << std::endl;

    int myArray[4] = {1,2,3,4};

    std::cout << "Average of an array " << average(myArray) << std::endl;

    std::cout << "Average of an array (templated function)" << average2<4>(myArray) << std::endl;

}

void doubleNumber(int& a)
{
    a *= 2;
}

int addTwo(const int& a, const int& b)
{
    return a + b;
}

float average(int (&myArray)[4])
{
    float acc;
    int count = 0;
    for(int x : myArray)
    {
        acc += x;
        count++;
    }
    return acc / count;
}

template <int T>
float average2(int (&myArray)[T])
{
    float acc;
    int count = 0;
    for(int x : myArray)
    {
        acc += x;
        count++;
    }
    return acc / count;
}